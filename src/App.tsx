import { useState } from 'react'
import { Icon } from '@iconify/react'
import Button from 'react-bootstrap/Button'
import reactLogo from '@/assets/react.svg'
import vscodeLogo from '@/assets/vscode.svg'
import typescriptLogo from '@/assets/typescript.svg'
import bootstrapLogo from '@/assets/bootstrap.svg'
import iconifyLogo from '@/assets/iconify.png'
import viteLogo from '/vite.svg'
import './App.css'

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
        <a href="https://code.visualstudio.com/" target="_blank">
          <img src={vscodeLogo} className="logo" alt="VSCode logo" />
        </a>
        <a href="https://www.typescriptlang.org/" target="_blank">
          <img src={typescriptLogo} className="logo" alt="TypeScript logo" />
        </a>
        <a href="https://getbootstrap.com/" target="_blank">
          <img src={bootstrapLogo} className="logo" alt="Bootstrap logo" />
        </a>
        <a href="https://iconify.design/" target="_blank">
          <img src={iconifyLogo} className="logo" alt="iconify logo" />
        </a>
      </div>
      <h2>Vite + React + VSCode + TypeScript(+SWC) + Bootstrap + iconify</h2>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.tsx</code> and save to test HMR
        </p>
        <Button variant="primary">Bootstrap</Button>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more<Icon icon="fa-solid:book" />
      </p>
    </>
  )
}

export default App
